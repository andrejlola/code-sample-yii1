<?php

/**
 * Class DeveloperSaleRateController
 */
class DeveloperSaleRateController extends Controller
{
    /**
     * @return array action filters
     */
    public function filters()
    {
        return [
            'accessControl',
        ];
    }

    /**
     * @link http://site.com/developr-sale-rates Displays page with datagrid
     *
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::app()->clientScript->registerCoreScript('jquery');
        FileHelper::loadFilesForJQGrid();
        $filter = [
            'filterWords' => $_GET['filterWords'],
        ];
        $dataProvider = new CActiveDataProvider('DeveloperSaleRate');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'filter' => $filter,
            'employeeId' => $_GET['employeeId']
        ));
    }

    /**
     * @link http://site.com/developr-sale-rates/get-list returns data for datagrid
     *
     * @return mixed
     */
    public function actionGetList()
    {
        $page = $_GET['page'];
        $limit = $_GET['rows'];
        $sortId = $_GET['sidx'];
        $sortOrder = $_GET['sord'];

        $portion = LDeveloperSaleRate::getPortion($page, $limit, $sortId, $sortOrder, $_GET['employeeId']);
        $response = new stdClass();
        $response->page = $portion->page;
        $response->total = $portion->totalPages;
        $response->records = $portion->count;
        $i = 0;
        /** @var LDeveloperSaleRate $model */
        foreach ($portion->models as $model) {
            $response->rows[$i]['id'] = $model->id;
            $response->rows[$i]['cell'] = [
                $model->getDate(),
                $model->rate_low,
                $model->rate_avg,
                $model->rate_high,
                $model->getCurrency()->name,
                $model->planned_day_time,
                $model->canBeChanged(),
            ];
            $i++;
        }
        echo json_encode($response);
    }

    /**
     * @link http://site.com/developr-sale-rates/create Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new LDeveloperSaleRate();
        if ($_POST) {
            $model->setAttributes($_POST['LDeveloperSaleRate']);
            if ($model->set()) {
                $this->redirect(array('index', 'employeeId' => $model->user_id));
            }
        } else {
            $model->user_id = $_GET['employeeId'];
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * @link http://site.com/developr-sale-rates/update Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if ($_POST) {
            $model->setAttributes($_POST['LDeveloperSaleRate']);
            if ($model->set()) {
                $this->redirect(array('index', 'employeeId' => $model->user_id));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * @link http://site.com/developr-sale-rates/delete Deletes rate
     */
    public function actionDelete()
    {
        $model = $this->loadModel($_GET['id']);
        $model->delete();
        $this->redirect($this->createUrl('index', ['employeeId' => $model->user_id]));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return LDeveloperSaleRate the loaded model
     * @throws CHttpException
     */
    protected function loadModel($id)
    {
        $model = LDeveloperSaleRate::model()
            ->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        return $model;
    }
}
