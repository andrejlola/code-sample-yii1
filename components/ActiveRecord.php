<?php

class ActiveRecord extends CActiveRecord
{
    use DateFormattedTrait;

    /** @var static */
    public $oldRecord;

    /**
     * @inheritdoc
     */
    protected function afterFind()
    {
        $this->oldRecord = clone $this;

        return parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public static function tblName()
    {
        return (new static())->tableName();
    }
}
