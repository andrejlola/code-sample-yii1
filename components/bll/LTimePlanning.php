<?php

class LTimePlanning extends ProjectDeveloperPlanningTime
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return LManager the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Returns list of possible years values
     *
     * @return array
     */
    public static function getYears()
    {
        $result = [];
        $current = date('Y');
        $to = new DateTime();
        if ($to->format('m') == 12) {
            $to->add(new DateInterval('P1Y'));
        }
        $to = $to->format('Y');
        while ($current <= $to) {
            $result[] = [
                'id' => $current
            ];
            $current++;
        }

        return $result;
    }

    /**
     * Returns list of possible months values
     *
     * @return array
     */
    public static function getMonths()
    {
        $result = [];
        $year = date('Y');
        $date = (new DateTime())->setDate($year, 1, 1);
        while ($year == $date->format('Y')) {
            $result[] = [
                'id' => $date->format('n'),
                'name' => $date->format('F'),
            ];
            $date->add(new DateInterval('P1M'));
        }

        return $result;
    }

    /**
     * Returns plan
     *
     * @param $year
     * @param $weekOfYear
     * @param $project
     * @param LDeveloper $developer
     * @return LTimePlanning
     */
    public static function getPlan($year, $weekOfYear, $project, LDeveloper $developer)
    {
        if ($project instanceof Project) {
            $result = self::model()
                ->find('year=:year and week=:week and project_id=:project_id and developer_id=:developer_id', [
                    ':year' => $year,
                    ':week' => $weekOfYear,
                    ':project_id' => $project->id,
                    ':developer_id' => $developer->id
                ]);
        } else {
            $result = MilestoneDeveloperPlanningTime::model()
                ->find('year=:year and week=:week and milestone_id=:milestone_id and developer_id=:developer_id', [
                    ':year' => $year,
                    ':week' => $weekOfYear,
                    ':milestone_id' => $project->id,
                    ':developer_id' => $developer->id
                ]);
        }

        return $result;
    }

    /**
     * Saves plan
     *
     * @param $year
     * @param $weekOfYear
     * @param $projectId
     * @param $projectType
     * @param $developerId
     * @param $time
     * @return LTimePlanning|MilestoneDeveloperPlanningTime
     */
    public static function setPlan($year, $weekOfYear, $projectId, $projectType, $developerId, $time)
    {
        if ($projectType == Project::TYPE_HOURLY) {
            if (!($model = self::model()
                ->find('year=:year and week=:week and project_id=:project_id and developer_id=:developer_id', [
                    ':year' => $year,
                    ':week' => $weekOfYear,
                    ':project_id' => $projectId,
                    ':developer_id' => $developerId
                ]))
            ) {
                $model = new self();
                $model->year = $year;
                $model->week = $weekOfYear;
                $model->project_id = $projectId;
                $model->developer_id = $developerId;
            }
            $model->manager_id = Yii::app()->user->id;
            $model->time = $time;
            $model->save();
        } else {
            $projectId = explode('.', $projectId);
            if (!($model = MilestoneDeveloperPlanningTime::model()
                ->find('year=:year and week=:week and milestone_id=:milestone_id and developer_id=:developer_id', [
                    ':year' => $year,
                    ':week' => $weekOfYear,
                    ':milestone_id' => $projectId[1],
                    ':developer_id' => $developerId
                ]))
            ) {
                $model = new MilestoneDeveloperPlanningTime();
                $model->year = $year;
                $model->week = $weekOfYear;
                $model->milestone_id = $projectId[1];
                $model->developer_id = $developerId;
            }
            $model->manager_id = Yii::app()->user->id;
            $model->time = $time;
            $model->save();
        }

        return $model;
    }

    /**
     * Sets actually worked out time for developer
     *
     * @param $date
     * @param $developerId
     * @param $time
     * @return ActuallyWorkedOutTime
     */
    public static function setActuallyWorkedOutTime($date, $developerId, $time)
    {
        if (!($model = ActuallyWorkedOutTime::model()
            ->find('date=:date and user_id=:user_id', [
                ':date' => $date,
                ':user_id' => $developerId
            ]))
        ) {
            $model = new ActuallyWorkedOutTime();
            $model->date = $date;
            $model->user_id = $developerId;
        }
        $model->manager_id = Yii::app()->user->id;
        $model->time = $time;
        $model->save();

        return $model;
    }
}
