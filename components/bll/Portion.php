<?php

/**
 * Class Portion
 */
class Portion
{
    public $models, $page, $totalPages, $count;

    /**
     * Portion constructor.
     * @param array $models
     * @param $page
     * @param $totalPages
     * @param $count
     */
    public function __construct(array $models, $page, $totalPages, $count)
    {
        $this->models = $models;
        $this->page = $page;
        $this->totalPages = $totalPages;
        $this->count = $count;
    }
}
