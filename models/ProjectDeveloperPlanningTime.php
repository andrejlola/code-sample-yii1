<?php

/**
 * This is the model class for table "obm_project_developer_planning_time".
 *
 * The followings are the available columns in table 'obm_project_developer_planning_time':
 * @property string $id
 * @property string $developer_id
 * @property string $project_id
 * @property integer $year
 * @property integer $week
 * @property double $time
 * @property string $manager_id
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property User $developer
 * @property User $manager
 */
class ProjectDeveloperPlanningTime extends ActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'obm_project_developer_planning_time';
    }


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('year, week, developer_id, project_id, time', 'filter', 'filter' => 'trim',),
            array(
                'time',
                'filter',
                'filter' => function ($value) {
                    return str_replace(',', '.', $value);
                }
            ),
            array('year, week, developer_id, project_id, time', 'required'),
            array('year, week, developer_id, project_id', 'numerical', 'integerOnly' => true),
            array('time', 'numerical', 'integerOnly' => false, 'min' => 0, 'max' => 100),
            array('year', 'numerical', 'integerOnly' => true, 'min' => 2017),
            array('week', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('developer_id, manager_id', 'length', 'max' => 10),
            array('id, developer_id, year, week, manager_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'project' => array(self::BELONGS_TO, 'Project', 'project_id'),
            'developer' => array(self::BELONGS_TO, 'User', 'developer_id'),
            'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'developer_id' => 'Разработчик',
            'year' => 'Год',
            'week' => 'Неделя',
            'manager_id' => 'Мэнеджер',
            'time' => 'Время',
            'project_id' => 'Проект',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProjectDeveloperPlanningTime the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
