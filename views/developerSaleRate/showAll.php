<?php
/* @var $this DeveloperSaleRateController */
$modelAttributes = (new LDeveloperSaleRate())->attributeLabels();
$firstLevel = 'Показатели рейтов разработчиков';
if (Yii::app()->user->isAuthority) {
    $this->breadcrumbs = array(
        Yii::t('app', $firstLevel) => $this->createUrl('index'),
    );
}
$nextLevel = '';
if (!Yii::app()->user->isAuthority) {
    $nextLevel = $firstLevel . ' ';
}
$this->breadcrumbs[] = $nextLevel . Yii::t('app', 'Все сотрудники (последняя запись)');
?>

<h1><?php echo Yii::t('app', 'Показатели рейтов разработчиков'); ?></h1>

<table id="list"></table>
<div id="pager"></div>

<script type="text/javascript">
    var colNames = [
        'Employee',
        '<?php echo Yii::t('app', $modelAttributes['date']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_low_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_avg_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_high_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['currency']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['planned_day_time']); ?>'
    ];
    var colModel = [
        {name: 'employee', index: 'employee', align: 'center', sortable: true, width: 20},
        {name: 'date', index: 'date', align: 'center', sortable: true, width: 20},
        {
            name: 'rate_low_transaction',
            index: 'rate_low_transaction',
            align: 'center',
            sortable: true,
            width: 20
        },
        {
            name: 'rate_avg_transaction',
            index: 'rate_avg_transaction',
            align: 'center',
            sortable: true,
            width: 20
        },
        {
            name: 'rate_high_transaction',
            index: 'rate_high_transaction',
            align: 'center',
            sortable: true,
            width: 20
        },
        {name: 'currency', index: 'currency', align: 'center', sortable: false, width: 20},
        {name: 'planned_day_time', index: 'planned_day_time', align: 'center', sortable: true, width: 20},
    ];
    jQuery("#list").jqGrid({
        url: '<?php echo $this->createUrl('developerSaleRate/getListAll', array()); ?>',
        datatype: "json",
        height: 500,
        autowidth: true,
        colNames: colNames,
        colModel: colModel,
        rowNum: 30,
        rowList: [10, 20, 30],
        pager: '#pager',
        sortname: 'date',
        sortorder: "desc"
    });
    jQuery("#list").jqGrid('navGrid', '#pager', {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: false,
        addfunc: false
    }, {}, {}, {}, {});
</script>
