<?php
/* @var $this DeveloperSaleRateController */
/* @var $model DeveloperSaleRate */

$this->breadcrumbs = [
    'Показатели рейтов разработчиков' => ['index'],
    'Изменить',
];
?>

<h1>Изменить</h1>

<?php $this->renderPartial('_form', [
    'model' => $model,
]); ?>
