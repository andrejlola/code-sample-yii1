<script type="text/JavaScript">
    function reload() {
        $('#list').trigger('reloadGrid');
    }
</script>
<?php
/* @var $this DeveloperSaleRateController */
$modelAttributes = (new LDeveloperSaleRate())->attributeLabels();
$this->breadcrumbs = array(Yii::t('app', 'Показатели рейтов разработчиков'),);
$createUrl = $this->createUrl('create');
$updateUrl = $this->createUrl('update');
$deleteUrl = $this->createUrl('delete');
?>

<?php
Yii::app()->clientScript->registerScript('search', '
$("#filterWords").keyup(function() {
  if (this.value.length > 2) {
    reload();
  }
});
');
?>
<h1><?php echo Yii::t('app', 'Показатели рейтов разработчиков'); ?></h1>

<table>
    <tr>
        <td><?php echo Yii::t('app', 'Работник'); ?></td>
    </tr>
    <tr>
        <td><?php echo CHtml::dropDownList('ddlEmployee', (empty($employeeId)
                ? null
                : $employeeId), AppHelper::prepareList(LManager::getSubordinates(), 'id', function ($model) {
                return $model->getLongName();
            }, false, true), ['onchange' => 'reload()']); ?></td>
        <td><a href="<?php echo $this->createUrl('showAll') ?>">Все сотрудники (последняя запись)</td>
    </tr>
</table>

<table id="list"></table>
<div id="pager"></div>

<script type="text/javascript">
    var colNames = [
        '<?php echo Yii::t('app', $modelAttributes['date']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_low_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_avg_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['rate_high_transaction_id']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['currency']); ?>',
        '<?php echo Yii::t('app', $modelAttributes['planned_day_time']); ?>',
        ''
    ];
    var colModel = [
        {name: 'date', index: 'date', align: 'center', sortable: false, width: 20},
        {
            name: 'rate_low_transaction_id',
            index: 'rate_low_transaction_id',
            align: 'center',
            sortable: false,
            width: 20
        },
        {
            name: 'rate_avg_transaction_id',
            index: 'rate_avg_transaction_id',
            align: 'center',
            sortable: false,
            width: 20
        },
        {
            name: 'rate_high_transaction_id',
            index: 'rate_high_transaction_id',
            align: 'center',
            sortable: false,
            width: 20
        },
        {name: 'currency', index: 'currency', align: 'center', sortable: false, width: 20},
        {name: 'planned_day_time', index: 'planned_day_time', align: 'center', sortable: false, width: 20},
        {name: 'can_be_changed', hidden: true, hidedlg: true}
    ];
    if (<?php echo LDeveloperSaleRate::updateAllowed()
        ? 'true'
        : 'false' ?>) {
        colNames.push('Действия');
        colModel.push({name: 'actions', align: 'center', sortable: false, width: 20});
    }
    jQuery("#list").jqGrid({
        url: '<?php echo $this->createUrl('developerSaleRate/getList', array()); ?>',
        datatype: "json",
        height: 500,
        autowidth: true,
        colNames: colNames,
        colModel: colModel,
        rowNum: 30,
        rowList: [10, 20, 30],
        pager: '#pager',
        sortname: 'date',
        sortorder: "desc",
        afterInsertRow: function (id, rowdata, jsondata) {
            if (rowdata.can_be_changed == 1) {
                $(this).setCell(id, 'actions',
                    '<a title="Изменить" href="<?php echo $updateUrl?>/?id=' + id + '"><img src="/img/small_update.png"></a>'
                    + ' <a title="Удалить" href="<?php echo $deleteUrl?>/?id=' + id + '" onclick="return confirm(\'Are you sure?\')"><img src="/img/small_delete.png"></a>'
                );
            }
        },
        serializeGridData: function (postData) {
            postData.employeeId = $('#ddlEmployee').val();
            return postData;
        }
    });
    jQuery("#list").jqGrid('navGrid', '#pager', {
        edit: false,
        add: <?php echo LDeveloperSaleRate::createAllowed()
        ? 'true'
        : 'false' ?>,
        del: false,
        search: false,
        refresh: false,
        addfunc: <?php echo LDeveloperSaleRate::createAllowed()
        ? <<<JS
function () {
    var url = '$createUrl';
	var employeeId = $('#ddlEmployee').val();
    if (employeeId != 0) {
		url += '?employeeId=' + employeeId; 
	}
    window.location.href = url;
}
JS
        : 'false' ?>
    }, {}, {}, {}, {});
</script>
